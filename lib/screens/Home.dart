import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:tomisha_frontend_test/models/AppColors.dart';
import 'package:tomisha_frontend_test/models/AppSizes.dart';
import 'package:tomisha_frontend_test/models/AppModel.dart';
import 'package:tomisha_frontend_test/screens/AppHero.dart';
import 'package:tomisha_frontend_test/screens/Arbeitgeber.dart';
import 'package:tomisha_frontend_test/screens/Arbeitnehmer.dart';
import 'package:tomisha_frontend_test/screens/Temporarburo.dart';
import 'package:tomisha_frontend_test/widgets/Header.dart';
import 'package:tomisha_frontend_test/widgets/TabFragments.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  int? groupValue = 0;
  double offsetX(int? value) {
    return (-(value ?? 0) * AppSizes.tabItemWidth + AppSizes.tabItemWidth).toDouble();
  }

  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 3, animationDuration: Duration(milliseconds: 100));
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget tabItem(int value, String text, AppModel model) {
    if (value == groupValue) {
      return Padding(
        padding: TabFragments.getTabPadding(value),
        child: GestureDetector(
          onTap: () {
            setState(() {
              groupValue = value;
              _tabController.index = value;
              model.changeTabIndex(value);
            });
          },
          child: Container(
            height: AppSizes.tabHeight,
            width: AppSizes.tabItemWidth,
            decoration: BoxDecoration(
              color: AppColors.tabActive,
              borderRadius: TabFragments.getBorderRadius(value),
            ),
            child: Center(
              child: Text(
                text,
                style: const TextStyle(
                    color: AppColors.white,
                    fontWeight: FontWeight.w600
                ),
              ),
            ),
          ),
        ),
      );
    } else {
      return Padding(
        padding: TabFragments.getTabPadding(value),
        child: GestureDetector(
          onTap: () {
            setState(() {
              groupValue = value;
              _tabController.index = value;
              model.changeTabIndex(value);
            });
          },
          child: Container(
            height: AppSizes.tabHeight,
            width: AppSizes.tabItemWidth,
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: TabFragments.getBorderRadius(value),
              border: TabFragments.getBorder(value),
            ),
            child: Center(
              child: Text(
                text,
                style: const TextStyle(
                    color: AppColors.heading,
                    fontWeight: FontWeight.w600
                ),
              ),
            ),
          ),
        ),
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => AppModel(),
      child: Stack(
        children: [
          Consumer<AppModel>(
            builder: (context, value, child) {
              return NotificationListener<ScrollUpdateNotification>(
                onNotification: (notification) {
                  if (notification.metrics.pixels > 200) {
                    value.changeScrolled(true);
                  } if (notification.metrics.pixels < 200) {
                    value.changeScrolled(false);
                  }
                  return true;
                },
                child: ListView(
                  children: [
                    Column(
                      children: [
                        AppHero(),
                        Container(
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            color: AppColors.white,
                          ),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 20,
                              bottom: 20,
                            ),
                            child: Consumer<AppModel>(
                              builder: (context, model, child) {
                                return TabBar(
                                  controller: _tabController,
                                  indicator: BoxDecoration(
                                  ),
                                  onTap: (value) {
                                    setState(() {
                                      groupValue = _tabController.index;
                                    });
                                  },
                                  isScrollable: true,
                                  labelPadding: EdgeInsets.all(0),
                                  tabs: [
                                    tabItem(0, 'Arbeitnehmer', model),
                                    tabItem(1, 'Arbeitgeber', model),
                                    tabItem(2, 'Temporärbüro', model),
                                  ],
                                );
                              }
                            ),
                          ),
                        ),
                        Consumer<AppModel>(
                          builder: (context, value, child) {
                            if (value.currentTabIndex == 0) {
                              return Arbeitnehmer();
                            } else if (value.currentTabIndex == 1) {
                              return Arbeitgeber();
                            } else {
                              return Temporarburo();
                            }
                          },
                        )
                      ],
                    ),
                  ],
                ),
              );
            }
          ),
          Positioned(
            child: Header(),
          ),
        ],
      ),
    );
  }
}
