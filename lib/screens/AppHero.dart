import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:tomisha_frontend_test/models/AppSizes.dart';
import 'package:tomisha_frontend_test/models/AppModel.dart';

import '../models/AppColors.dart';

class AppHero extends StatefulWidget {
  const AppHero({Key? key,}) : super(key: key);

  @override
  State<AppHero> createState() => _AppHeroState();
}

class _AppHeroState extends State<AppHero> {
  double getSvgSize(BuildContext context) {
    if (ResponsiveWrapper.of(context).isLargerThan(DESKTOP)) {
      return AppSizes.fixedDesktopAppHeroHeightWidth;
    } else if (ResponsiveWrapper.of(context).isLargerThan(TABLET)) {
    return AppSizes.fixedTabletAppHeroHeightWidth;
    } else if (ResponsiveWrapper.of(context).isLargerThan('LargeMobile')) {
      return AppSizes.fixedMobileAppHeroHeightWidth;
    } else if (ResponsiveWrapper.of(context).isLargerThan(MOBILE)) {
      return AppSizes.fixedMobileAppHeroHeightWidth;
    } else {
      return MediaQuery.of(context).size.width;
    }
  }

  double getTitleSize(BuildContext context) {
    if(ResponsiveWrapper.of(context).isLargerThan(DESKTOP)) {
      return 72;
    } else if (ResponsiveWrapper.of(context).isLargerThan(TABLET)) {
      return 66;
    } else if (ResponsiveWrapper.of(context).isLargerThan(MOBILE)) {
      return 50;
    } else {
      return 40;
    }
  }

  Widget getSvgBackground(BuildContext context, Widget child) {
    if (ResponsiveWrapper.of(context).isLargerThan(MOBILE)) {
      return Container(
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: AppColors.white,
        ),
      );
    } else {
      return Container(
        child: child,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (true) {
          return Stack(
            children: [
              Container(
                height: ResponsiveWrapper.of(context).isLargerThan(TABLET)
                ? MediaQuery.of(context).size.height / 1.5
                : MediaQuery.of(context).size.height,
                constraints: BoxConstraints(
                  maxHeight: 800
                ),
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: <Color>[
                      AppColors.backgroundPrimary,
                      AppColors.backgroundSecondary
                    ],
                    begin: Alignment(0.1,0),
                    end: Alignment(0.1,-1),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: AppSizes.headerHeight),
                  child: ResponsiveRowColumn(
                    layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                    ? ResponsiveRowColumnType.COLUMN
                    : ResponsiveRowColumnType.ROW,
                    rowMainAxisSize: MainAxisSize.max,
                    rowMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    rowCrossAxisAlignment: CrossAxisAlignment.center,
                    columnMainAxisAlignment: MainAxisAlignment.start,
                    columnCrossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ResponsiveRowColumnItem(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 85, vertical: 18),
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 40),
                                child: Text(
                                  'Deine Job website',
                                  style: TextStyle(
                                    fontWeight: ResponsiveWrapper.of(context).isLargerThan(TABLET)
                                      ? FontWeight.bold
                                      : FontWeight.w500,
                                    fontSize: getTitleSize(context),
                                    color: AppColors.title
                                  ),
                                  textAlign: ResponsiveWrapper.of(context).isLargerThan(DESKTOP)
                                  ? TextAlign.start
                                  : TextAlign.center,
                                ),
                              ),
                            ),
                            Builder(
                              builder: (context) {
                                if (ResponsiveWrapper.of(context).isLargerThan(TABLET)) {
                                  return Container(
                                    width: 500,
                                    decoration: const BoxDecoration(
                                      gradient: LinearGradient(
                                       colors: [
                                         AppColors.heading,
                                         AppColors.buttonSecondary
                                       ],
                                       begin: Alignment(-1,0),
                                       end: Alignment(1,0)
                                     ),
                                      borderRadius: BorderRadius.all(Radius.circular(12))
                                    ),
                                    child: const Padding(
                                      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
                                      child: Text(
                                        'Kostenlos Registrieren',
                                        style: TextStyle(
                                          color: AppColors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  );
                                } else {
                                  return const SizedBox();
                                }
                              },
                            )
                          ],
                        ),
                      ),
                      ResponsiveRowColumnItem(
                        child: Container(
                          decoration: ResponsiveWrapper.of(context).isLargerThan(MOBILE)
                            ? const BoxDecoration(
                              color: AppColors.white,
                              shape: BoxShape.circle,
                            )
                            : const BoxDecoration(),
                          constraints: const BoxConstraints(
                            maxWidth: AppSizes.fixedTabletAppHeroHeightWidth,
                          ),
                          child: Transform.scale(
                            scale: ResponsiveWrapper.of(context).isLargerThan(MOBILE)
                            ? 1
                            : 1.3,
                            child: Transform.translate(
                              offset: const Offset(20, 0),
                              child: SvgPicture.asset(
                                'assets/images/AppHero.svg',
                                width: getSvgSize(context),
                                height: getSvgSize(context),
                              )
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Builder(
                builder: (context) {
                  if (ResponsiveWrapper.of(context).isLargerThan(TABLET)) {
                    return Positioned(
                      bottom: 0,
                      child: SvgPicture.asset(
                        'assets/images/RectangleTriangle.svg',
                        width: MediaQuery.of(context).size.width,
                      ),
                    );
                  } else {
                    return Consumer<AppModel>(
                      builder: (context, value, child) {
                        if (!value.scrolled) {
                          return Positioned(
                            bottom: 0,
                            child: Container(
                              width: constraints.maxWidth,
                              decoration: const BoxDecoration(
                                  color: AppColors.white,
                                  borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, .2),
                                        offset: Offset(0, -3),
                                        blurRadius: 3
                                    )
                                  ]
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      gradient: LinearGradient(
                                          colors: [
                                            AppColors.heading,
                                            AppColors.buttonSecondary
                                          ],
                                          begin: Alignment(-1,0),
                                          end: Alignment(1,0)
                                      )
                                  ),
                                  child: const Padding(
                                    padding: EdgeInsets.symmetric(vertical: 16),
                                    child: Text(
                                      'Kostenlos Registrieren',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: AppColors.white,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          );
                        } else {
                          return SizedBox();
                        }
                      },
                    );
                  };
                },
              ),
            ],
          );
        }
      }
    );
  }
}
