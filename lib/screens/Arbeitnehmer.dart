import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tomisha_frontend_test/models/AppSizes.dart';
import 'package:tomisha_frontend_test/widgets/TabFragments.dart';
class Arbeitnehmer extends StatelessWidget {
  const Arbeitnehmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TabFragments fragments = TabFragments(context);
    return LayoutBuilder(
      builder: (context, constraints) {
        if (true) {
          return Column(
            children: [
              fragments.firstWidget('Drei einfache Schritte zu deinem neuen Job', 'Erstellen dein Lebenslauf'),
              fragments.secondWidget('Erstellen dein Lebenslauf', 'assets/images/Arbeitnehmer2.svg', AppSizes.arbeitnehmer2Height, AppSizes.arbeitnehmer2Width),
              fragments.thirdWidget('Mit nur einem Klick bewerben', 'assets/images/Arbeitnehmer3.svg')
            ],
          );
        }
      }
    );
  }
}
