import 'package:flutter/material.dart';
import 'package:tomisha_frontend_test/models/AppSizes.dart';
import 'package:tomisha_frontend_test/widgets/TabFragments.dart';

class Temporarburo extends StatelessWidget {
  const Temporarburo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TabFragments fragments = TabFragments(context);
    return LayoutBuilder(
      builder: (context, constraints) {
        return Column(
          children: [
            fragments.firstWidget('Drei einfache Schritte zur Vermittlung neuer Mitarbeiter', 'Erstellen dein Unternehmensprofil'),
            fragments.secondWidget('Erhalte Vermittlungs- angebot von Arbeitgeber', 'assets/images/Temporarburo2.svg', AppSizes.temporarburo2Height, AppSizes.temporarburo2Width),
            fragments.thirdWidget('Vermittlung nach Provision oder Stundenlohn', 'assets/images/Temporarburo3.svg')
          ],
        );
      }
    );
  }
}
