import 'package:flutter/material.dart';
import 'package:tomisha_frontend_test/models/AppSizes.dart';
import 'package:tomisha_frontend_test/widgets/TabFragments.dart';

class Arbeitgeber extends StatelessWidget {
  const Arbeitgeber({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TabFragments fragments = TabFragments(context);
    return LayoutBuilder(
      builder: (context, constraints) {
        return Column(
          children: [
            fragments.firstWidget('Drei einfache Schritte zu deinem neuen Mitarbeiter', 'Erstellen dein Unternehmensprofil'),
            fragments.secondWidget('Erstellen ein Jobinserat', 'assets/images/Arbeitgeber2.svg', AppSizes.arbeitgeber2Height, AppSizes.arbeitgeber2Width),
            fragments.thirdWidget('Wähle deinen neuen Mitarbeiter aus', 'assets/images/Arbeitgeber3.svg'),
          ],
        );
      }
    );
  }
}
