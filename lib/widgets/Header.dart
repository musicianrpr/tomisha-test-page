import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tomisha_frontend_test/models/AppColors.dart';
import 'package:tomisha_frontend_test/models/AppSizes.dart';
import 'package:tomisha_frontend_test/models/AppModel.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.backgroundSecondary,
      child: Container(
        decoration: const BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(12)),
          boxShadow: [
            BoxShadow(
              color: AppColors.shadow,
              blurRadius: 6,
              offset: Offset(0, 4),
            ),
          ]
        ),
        height: AppSizes.headerHeight,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Consumer<AppModel>(
                builder: (context, value, child) {
                  if (value.scrolled) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          color: AppColors.buttonSecondary
                        ),
                        height: 100,
                        child: const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text('Kostenlos Registrieren', style: TextStyle(
                              color: AppColors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    );
                  } else {
                    return const SizedBox();
                  }
                },
              ),
              GestureDetector(
                child: const Text(
                  'Login',
                  style: TextStyle(
                    color: AppColors.heading,
                    fontWeight: FontWeight.bold
                  )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
