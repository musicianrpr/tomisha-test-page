import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:tomisha_frontend_test/models/AppColors.dart';
import 'package:tomisha_frontend_test/models/AppSizes.dart';

class TabFragments {
  BuildContext context;

  TabFragments(this.context);
  String resizeCondition = 'LargeMobile';

  bool screenIsLargerThan(String name) {
    return ResponsiveWrapper.of(context).isLargerThan(name);
  }

  double getSize(double size) {
    if (screenIsLargerThan(DESKTOP)) {
      return size * 1.8;
    } else if (screenIsLargerThan(TABLET)) {
      return size * 1.4;
    } else if (screenIsLargerThan(resizeCondition)) {
      return size * 1.2;
    } else if (screenIsLargerThan(MOBILE)){
      return size;
    } else {
      return size * .8;
    }
  }

  EdgeInsets getPadding(bool isColumn) {
    if (isColumn) {
      return EdgeInsets.zero;
    } else {
      return const EdgeInsets.symmetric(horizontal: 40);
    }
  }

  static EdgeInsets getTabPadding(int value) {
    if (value == 0) {
      return EdgeInsets.only(left: 20);
    } else if (value == 1) {
      return EdgeInsets.zero;
    } else {
      return EdgeInsets.only(right: 20);
    }
  }

  double getTitleSize(BuildContext context) {
    if (screenIsLargerThan(DESKTOP)) {
      return 130;
    } else if (screenIsLargerThan(TABLET)) {
      return 120;
    } else if (screenIsLargerThan(MOBILE)) {
      return 110;
    } else {
      return 110;
    }
  }

  Widget numberTitle(String text, double offset, BuildContext context) {
    return Transform.translate(
      offset: Offset(0, offset),
      child: Text(
        text,
        style: TextStyle(
            fontSize: getTitleSize(context),
            color: AppColors.heading1Number,
          height: .9
        ),
      ),
    );
  }

  double getFontSize() {
    if (screenIsLargerThan(DESKTOP)) {
      return 40;
    } else if (screenIsLargerThan(TABLET)) {
      return 30;
    } else if (screenIsLargerThan(resizeCondition)) {
      return 22;
    } else if (screenIsLargerThan(MOBILE)) {
      return 18;
    } else {
      return 14;
    }
  }

  Widget titleDescription(String text, bool atLeft) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: Container(
        constraints: screenIsLargerThan(MOBILE) ? BoxConstraints(maxWidth: AppSizes.maxTabletDescriptionWidth) : BoxConstraints(maxWidth: AppSizes.maxDescriptionWidth),
        child: Text(
          text,
          style: TextStyle(
              color: AppColors.heading1Number,
              fontSize: getFontSize(),
              fontWeight: FontWeight.w500
          ),
        ),
      ),
    );
  }

  Widget firstWidget(String title, String description) {
    return Container(
      color: AppColors.white,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 14),
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: AppColors.subTitle,
                fontSize: screenIsLargerThan(resizeCondition)
                ? 32
                : 21,
                fontWeight: FontWeight.w500,

              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 20,
              bottom: screenIsLargerThan(resizeCondition)
                ? 60
                : 0,
              top: screenIsLargerThan(resizeCondition)
                ? 60
                : 0,
            ),
            child: Align(
              alignment: const Alignment(-0.5, 0),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  numberTitle('1.', AppSizes.tabFragmentTitleStandardOffset, context),
                  SizedBox(
                    child: ResponsiveRowColumn(
                      rowCrossAxisAlignment: CrossAxisAlignment.end,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      layout: screenIsLargerThan(resizeCondition)
                      ? ResponsiveRowColumnType.ROW
                      : ResponsiveRowColumnType.COLUMN,
                      columnMainAxisAlignment: MainAxisAlignment.end,
                      columnCrossAxisAlignment: CrossAxisAlignment.start,
                      columnVerticalDirection: VerticalDirection.up,
                      children: [
                        ResponsiveRowColumnItem(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 50),
                            child: titleDescription(description, true),
                          ),
                        ),
                        ResponsiveRowColumnItem(
                          child: Padding(
                            padding: getPadding(!screenIsLargerThan(resizeCondition)),
                            child: SvgPicture.asset(
                              'assets/images/Arbeitnehmer1.svg',
                              height: getSize(AppSizes.arbeitnehmer1Height),
                              width: getSize(AppSizes.arbeitnehmer1Width),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget secondWidget(String description, String imagePath, double imageHeight, double imageWidth) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Stack(
          children: [
            Container(
              width: double.infinity,
              alignment: Alignment(0.5,0),
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    AppColors.backgroundPrimary,
                    AppColors.backgroundSecondary
                  ],
                  begin: Alignment(-0.7, -1),
                  end: Alignment(0.7, 1),
                )
              ),
              child: Padding(
                padding: EdgeInsets.only(
                  bottom: screenIsLargerThan(resizeCondition)
                      ? 140
                      : 90,
                  top: screenIsLargerThan(resizeCondition)
                      ? 110
                      : 60,
                ),
                child: ResponsiveRowColumn(
                  layout: screenIsLargerThan(resizeCondition)
                  ? ResponsiveRowColumnType.ROW
                  : ResponsiveRowColumnType.COLUMN,
                  columnMainAxisSize: MainAxisSize.min,
                  rowMainAxisAlignment: MainAxisAlignment.center,
                  rowTextDirection: TextDirection.rtl,
                  children: [
                    ResponsiveRowColumnItem(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            numberTitle('2.', AppSizes.tabFragmentTitleStandardOffset, context),
                            titleDescription(description, false)
                          ],
                        ),
                      ),
                    ),
                    ResponsiveRowColumnItem(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 24),
                        child: Transform.translate(
                          offset: Offset(0, imagePath == 'assets/images/Arbeitgeber2.svg' ? -50 : 0),
                          child: Padding(
                            padding: getPadding(!screenIsLargerThan(resizeCondition)),
                            child: SvgPicture.asset(
                              imagePath,
                              height: getSize(imageHeight),
                              width: getSize(imageWidth),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 0,
              child: Transform.translate(
                offset: screenIsLargerThan(TABLET) ? Offset(0, -40) : Offset.zero,
                child: Transform.rotate(
                  angle: pi,
                  child: SvgPicture.asset('assets/images/wave.svg', height: constraints.maxWidth/12, width: 0,),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Transform.translate(
                offset: screenIsLargerThan(TABLET) ? Offset(0, 40) : Offset.zero,
                child: SvgPicture.asset('assets/images/wave.svg', height: constraints.maxWidth/12)
              ),
            ),
          ],
        );
      }
    );
  }
  Widget thirdWidget(String description, String imagePath) {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.only(
          bottom: screenIsLargerThan(resizeCondition)
              ? 60
              : 0,
          top: screenIsLargerThan(resizeCondition)
              ? 60
              : 0,
        ),
        child: Stack(
          children: [
            Transform.translate(
              offset: Offset(0,-50),
              child: Align(
                alignment: screenIsLargerThan(TABLET)
                  ? Alignment(-0.5,-1)
                  : Alignment(-.9, -1),
                child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    color: AppColors.temporarburoCircle,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: ResponsiveRowColumn(
                layout: screenIsLargerThan(resizeCondition)
                ? ResponsiveRowColumnType.ROW
                : ResponsiveRowColumnType.COLUMN,
                columnMainAxisSize: MainAxisSize.min,
                rowMainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ResponsiveRowColumnItem(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        numberTitle('3.', 0, context),
                        titleDescription(description, true)
                      ],
                    ),
                  ),
                  ResponsiveRowColumnItem(
                    child: Transform.translate(
                      offset: Offset(0, imagePath == 'assets/images/Arbeitnehmer3.svg' ? -30 : 0),
                      child: Padding(
                        padding: getPadding(!screenIsLargerThan(TABLET)),
                        child: SvgPicture.asset(
                          imagePath,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  static BorderSide get borderSide => const BorderSide(
    color: AppColors.border,
    width: 2,
  );

  static BorderRadius? getBorderRadius(int value) {
    if (value == 0) {
      return const BorderRadius.only(bottomLeft: Radius.circular(AppSizes.tabRadius), topLeft: Radius.circular(AppSizes.tabRadius));
    } else if (value == 2) {
      return const BorderRadius.only(bottomRight: Radius.circular(AppSizes.tabRadius), topRight: Radius.circular(AppSizes.tabRadius));
    } else {
      return null;
    }
  }

  static Border getBorder(int value) {
    if (value == 1) {
      return const Border.symmetric(
        horizontal: BorderSide(
            color: AppColors.border,
            width: 2
        ),
      );
    } else if (value == 0) {
      return Border.all(
          width: 2,
          color: AppColors.border
      );
    } else {
      return Border.all(
          width: 2,
          color: AppColors.border
      );
    }
  }


}