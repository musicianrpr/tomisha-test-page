import 'dart:ui';

class AppColors {
  static const Color backgroundPrimary = Color(0xFFEBF4FF);
  static const Color backgroundSecondary = Color(0xFFE6FFFA);
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color.fromRGBO(0, 0, 0, 1);
  static const Color shadow = Color(0x29000000);
  static const Color heading = Color(0xFF319795);
  static const Color buttonSecondary = Color(0xFF3182CE);
  static const Color title = Color(0xFF2D3748);
  static const Color tabActive = Color(0xFF81E6D9);
  static const Color border = Color(0xFFCBD5E0);
  static const Color subTitle = Color(0xFF4A5568);
  static const Color heading1Number = Color(0xFF718096);
  static const Color temporarburoCircle = Color(0xFFF7FAFC);
}