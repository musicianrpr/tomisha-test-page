class AppSizes {
  static const double headerHeight = 67;
  static const double tabItemWidth = 160.5;
  static const double tabHeight = 40;
  static const double tabRadius = 12;
  static const double arbeitnehmer1Height = 144;
  static const double arbeitnehmer1Width = 220;
  static const double arbeitnehmer2Height = 140;
  static const double arbeitnehmer2Width = 181;
  static const double arbeitgeber2Height = 198;
  static const double arbeitgeber2Width = 137;
  static const double temporarburo2Height = 148;
  static const double temporarburo2Width = 218;
  static const double tabFragmentTitleStandardOffset = 15;
  static const double maxDescriptionWidth = 190;
  static const double maxTabletDescriptionWidth = 340;
  static const double fixedDesktopAppHeroHeightWidth = 755;
  static const double fixedTabletAppHeroHeightWidth = 455;
  static const double fixedMobileAppHeroHeightWidth = 400;
}
