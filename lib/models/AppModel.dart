import 'package:flutter/cupertino.dart';

class AppModel extends ChangeNotifier {
  bool scrolled = false;
  int currentTabIndex = 0;

  void changeTabIndex(int index) {
    print(index);
    currentTabIndex = index;
    notifyListeners();
  }

  void changeScrolled(bool value) {
    scrolled = value;
    notifyListeners();
  }
}