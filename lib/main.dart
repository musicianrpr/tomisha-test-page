import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:tomisha_frontend_test/models/AppColors.dart';
import 'package:tomisha_frontend_test/screens/Home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Demo',
      builder: (context, widget) => ResponsiveWrapper.builder(
        ClampingScrollWrapper.builder(context, widget!),
        breakpoints: const [
          ResponsiveBreakpoint.autoScaleDown(350, name: MOBILE),
          ResponsiveBreakpoint.resize(450, name: 'LargeMobile'),
          ResponsiveBreakpoint.resize(790, name: TABLET),
          ResponsiveBreakpoint.resize(1200, name: DESKTOP),
          ResponsiveBreakpoint.resize(1920, name: 'XL')
        ]
      ),
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Lato',
        backgroundColor: AppColors.black
      ),
      home: const MyHomePage(title: 'Tomisha Test Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Flexible(
              child: Home()
            ),
          ],
        ),
      ),
    );
  }
}
